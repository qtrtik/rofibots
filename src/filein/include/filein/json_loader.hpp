#ifndef JSON_LOADER_INCLUDED
#define JSON_LOADER_INCLUDED

#include <boost/json.hpp>
#include <utils/math.hpp>

#include <filesystem>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>

void save_configuration(const boost::json::value &value, const std::filesystem::path directory, const char* file_name);
const boost::json::value read_file(const std::filesystem::path &filename);
glm::mat4 get_transformation_matrix(const boost::json::value &matrix_array);
void pretty_print( std::ostream& os, boost::json::value const& jv, std::string* indent = nullptr );

#endif