#ifndef OBJ_LOADER_INCLUDED
#define OBJ_LOADER_INCLUDED

#include <gfx/mesh.hpp>

#include <filesystem>
#include <memory>
#include <string>
#include <stdexcept>
#include <vector>

using mesh_ptr = std::shared_ptr<Mesh>;

mesh_ptr load_object(std::filesystem::path path, bool normalize = false, bool centralize = true);

#endif