#ifndef TEXTURE_LOADER_INCLUDED
#define TEXTURE_LOADER_INCLUDED

#include <gfx/texture.hpp>
#include <osi/opengl.hpp>

#include <filesystem>
#include <vector>

texture_ptr load_texture_2d(const std::filesystem::path filename);
texture_ptr load_texture_cube_map(const std::vector<std::filesystem::path> &paths);

#endif