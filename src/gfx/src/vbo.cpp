#include <gfx/vbo.hpp>

#include <osi/opengl.hpp>

#include <vector>

VBO::VBO()
{
    glCreateBuffers(1, &ID);
    ASSUMPTION(glGetError() == GL_NO_ERROR);
    ASSUMPTION(ID != 0);
}

VBO::~VBO()
{
    deleteBuffer();
}

void VBO::deleteBuffer()
{
    if (ID != 0)
    {
        glDeleteBuffers(1, &ID);
        ASSUMPTION(glGetError() == GL_NO_ERROR);
        ID = 0;
    }
}

void VBO::sendData(std::vector<float> const &vertices)
{
    ASSUMPTION(ID != 0);
    glNamedBufferStorage(ID, sizeof(float) * vertices.size(), vertices.data(), GL_DYNAMIC_STORAGE_BIT);
    ASSUMPTION(glGetError() == GL_NO_ERROR);
}
