#include <gfx/ebo.hpp>
#include <osi/opengl.hpp>

#include <vector>

EBO::EBO()
{
    glCreateBuffers(1, &ID);
    ASSUMPTION(glGetError() == GL_NO_ERROR);
    ASSUMPTION(ID != 0);
}

EBO::~EBO()
{
    deleteBuffer();
}

void EBO::deleteBuffer()
{
    if (ID != 0)
    {
        glDeleteBuffers(1, &ID);
        ASSUMPTION(glGetError() == GL_NO_ERROR);
        ID = 0;
    }
}

void EBO::sendData(std::vector<unsigned int> const &indices)
{
    ASSUMPTION(ID != 0);
    glNamedBufferStorage(ID, sizeof(unsigned int) * indices.size(), indices.data(), GL_DYNAMIC_STORAGE_BIT);
    ASSUMPTION(glGetError() == GL_NO_ERROR);
}
