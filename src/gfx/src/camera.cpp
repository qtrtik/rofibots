#include <gfx/camera.hpp>

Camera::Camera(float _FOV, node_weak_ptr _node) : FOV{_FOV}, node{_node} {}


float Camera::getFOV() const { return FOV; }
void Camera::setFOV(float _FOV) { FOV = _FOV <= 120 ? _FOV : 120; }

glm::vec2 Camera::getWindowSize() const
{
    return window_size;
}

void Camera::setWindowSize(glm::u32vec2 _window_size)
{
    window_size = _window_size;
}