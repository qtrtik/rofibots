#include <gfx/texture.hpp>

#include <filein/texture_loader.hpp>

texture_ptr Texture::loadTexture(const std::filesystem::path texture_file)
{
    return load_texture_2d(texture_file);
}

texture_ptr Texture::loadTexture(const std::vector<std::filesystem::path> &cube_map_texture_files)
{
    return load_texture_cube_map(cube_map_texture_files);
}