#include <gfx/shapes.hpp>

namespace shapes
{
    
std::vector<float> generate_grid_vertices(int span)
{
    std::vector<float> vertices;

    for (int i = -span; i <= span; ++i)
    {
        for(int j = 1; j >= -1; j -= 2)
        {
            vertices.emplace_back((float)i + 0.5f);
            vertices.emplace_back(0.0f);
            vertices.emplace_back((float)(span * j) + 0.5f);
        }
        for(int j = 1; j >= -1; j -= 2)
        {
            vertices.emplace_back((float)(span * j) + 0.5f);
            vertices.emplace_back(0.0f);
            vertices.emplace_back((float)i + 0.5f);
        }
    }

    return vertices;
}

std::vector<unsigned int> generate_grid_indices(int span)
{
    std::vector<unsigned int> indices;
    for (int i = 0; i < 8 * span + 4; ++i)
        indices.emplace_back(i);
    return indices;
}

}