#include <gfx/constants.hpp>

const unsigned int SHD_MODEL_MAT = 0;
const unsigned int SHD_VIEW_MAT = 1;
const unsigned int SHD_PROJ_MAT = 2;

const unsigned int SHD_OBJECT_COLOR = 3;
const unsigned int SHD_TEXTURE_BOOL = 4;

// Buffer binding
const unsigned int SHD_CAMERA = 0;
const unsigned int SHD_OBJECT_MATERIAL = 1;
const unsigned int SHD_LIGHTS = 2;
const unsigned int SHD_SKYBOX_TEX = 3;
const unsigned int SHD_ALBEDO_TEX = 4;