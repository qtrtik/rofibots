#ifndef SHAPES_INCLUDED
#define SHAPES_INCLUDED

#include <vector>

namespace shapes
{

std::vector<float> generate_grid_vertices(int span);
std::vector<unsigned int> generate_grid_indices(int span);

static std::vector<float> obj_vertices =
{
    // positions
    //front
    0.2f, 0.2f, 0.2f,   // top right
    0.2f, -0.2f, 0.2f,   // bottom right
    -0.2f, -0.2f, 0.2f,  // bottom left
    -0.2f, 0.2f, 0.2f,  // top left
    //back
    0.2f, 0.2f, -0.2f,   // top right
    0.2f, -0.2f, -0.2f,  // bottom right
    -0.2f, -0.2f, -0.2f,  // bottom left
    -0.2f, 0.2f, -0.2f  // top left
};

static std::vector<unsigned int> obj_indices =
    {
       // front
       0, 1, 3, 1, 2, 3,
       // back
       4, 5, 7, 5, 6, 7,
       // right
       0, 1, 4, 1, 4, 5,
       // left
       2, 3, 7, 2, 6, 7,
       // top
       0, 3, 4, 3, 4, 7,
       // bottom
       1, 2, 5, 2, 5, 6
    };

static std::vector<float> lit_cube_vertices_big =
{
    -1.0f,  1.0f, -1.0f, 1.0f,  1.0f, -1.0f, -1.0f,  1.0f,  1.0f, 1.0f,  1.0f,  1.0f, // +Y
    -1.0f,  -1.0f, -1.0f, 1.0f,  -1.0f, -1.0f, -1.0f,  -1.0f,  1.0f, 1.0f,  -1.0f,  1.0f, // -Y
        1.0f,  1.0f,  1.0f, 1.0f,  1.0f, -1.0f, 1.0f, -1.0f,  1.0f, 1.0f, -1.0f, -1.0f, // +X
    -1.0f,  1.0f,  1.0f, -1.0f,  1.0f, -1.0f, -1.0f, -1.0f,  1.0f, -1.0f, -1.0f, -1.0f, // -X
    -1.0f,  1.0f, 1.0f, 1.0f,  1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, // +Z
    -1.0f,  1.0f, -1.0f, 1.0f,  1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f // -Z
};

static std::vector<float> lit_cube_vertices =
{
    -0.5f,  0.5f, -0.5f, 0.5f,  0.5f, -0.5f, -0.5f,  0.5f,  0.5f, 0.5f,  0.5f,  0.5f, // +Y
    -0.5f,  -0.5f, -0.5f, 0.5f,  -0.5f, -0.5f, -0.5f,  -0.5f,  0.5f, 0.5f,  -0.5f,  0.5f, // -Y
        0.5f,  0.5f,  0.5f, 0.5f,  0.5f, -0.5f, 0.5f, -0.5f,  0.5f, 0.5f, -0.5f, -0.5f, // +X
    -0.5f,  0.5f,  0.5f, -0.5f,  0.5f, -0.5f, -0.5f, -0.5f,  0.5f, -0.5f, -0.5f, -0.5f, // -X
    -0.5f,  0.5f, 0.5f, 0.5f,  0.5f, 0.5f, -0.5f, -0.5f, 0.5f, 0.5f, -0.5f, 0.5f, // +Z
    -0.5f,  0.5f, -0.5f, 0.5f,  0.5f, -0.5f, -0.5f, -0.5f, -0.5f, 0.5f, -0.5f, -0.5f // -Z
};

static std::vector<float> voxel_cube_vertices =
{
    -0.4f,  0.4f, -0.4f, 0.4f,  0.4f, -0.4f, -0.4f,  0.4f,  0.4f, 0.4f,  0.4f,  0.4f, // +Y
    -0.4f,  -0.4f, -0.4f, 0.4f,  -0.4f, -0.4f, -0.4f,  -0.4f,  0.4f, 0.4f,  -0.4f,  0.4f, // -Y
        0.4f,  0.4f,  0.4f, 0.4f,  0.4f, -0.4f, 0.4f, -0.4f,  0.4f, 0.4f, -0.4f, -0.4f, // +X
    -0.4f,  0.4f,  0.4f, -0.4f,  0.4f, -0.4f, -0.4f, -0.4f,  0.4f, -0.4f, -0.4f, -0.4f, // -X
    -0.4f,  0.4f, 0.4f, 0.4f,  0.4f, 0.4f, -0.4f, -0.4f, 0.4f, 0.4f, -0.4f, 0.4f, // +Z
    -0.4f,  0.4f, -0.4f, 0.4f,  0.4f, -0.4f, -0.4f, -0.4f, -0.4f, 0.4f, -0.4f, -0.4f // -Z
};

static std::vector<float> connector_cube_vertices =
{
    -0.1f,  0.1f, -0.1f, 0.1f,  0.1f, -0.1f, -0.1f,  0.1f,  0.1f, 0.1f,  0.1f,  0.1f, // +Y
    -0.1f,  -0.1f, -0.1f, 0.1f,  -0.1f, -0.1f, -0.1f,  -0.1f,  0.1f, 0.1f,  -0.1f,  0.1f, // -Y
        0.1f,  0.1f,  0.1f, 0.1f,  0.1f, -0.1f, 0.1f, -0.1f,  0.1f, 0.1f, -0.1f, -0.1f, // +X
    -0.1f,  0.1f,  0.1f, -0.1f,  0.1f, -0.1f, -0.1f, -0.1f,  0.1f, -0.1f, -0.1f, -0.1f, // -X
    -0.1f,  0.1f, 0.1f, 0.1f,  0.1f, 0.1f, -0.1f, -0.1f, 0.1f, 0.1f, -0.1f, 0.1f, // +Z
    -0.1f,  0.1f, -0.1f, 0.1f,  0.1f, -0.1f, -0.1f, -0.1f, -0.1f, 0.1f, -0.1f, -0.1f // -Z
};

static std::vector<unsigned int> lit_cube_indices =
{
    8, 9, 10, 9, 11, 10,
    14, 13, 12, 14, 15, 13,
    1, 2, 0, 3, 2, 1,
    4, 6, 5, 5, 6, 7,
    17, 18, 16, 19, 18, 17,
    20, 22, 21, 21, 22, 23
};

static std::vector<float> lit_cube_normals =
{
    0.0f, 1.0f, 0.0f,  0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
    0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f,  0.0f, -1.0f, 0.0f, 
    1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
    -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
    0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f
};

// Triangle
static std::vector<float> triangle_vertices = { 0, -0.2f, 0.5f,  0.7f, 0.3f, 0,  -0.5f, 0.5f, 1 };
static std::vector<unsigned int> triangle_indices = { 0, 1, 2 };

// Axis object
static std::vector<float> axis_obj_vertices = 
        {
          0, 0, 0,       1, -0.2f, 0,       1, 0.2f, 0,
          0, 0, 0,   -0.2f,     2, 0,    0.2f,    2, 0,
          0, 0, 0,   -0.2f,     0, 3,    0.2f,    0, 3,
        };

static std::vector<unsigned int> axis_obj_indices = 
        { 0, 1, 2,
          3, 4, 5,
          6, 7, 8
        };
static std::vector<float> axis_obj_normals = 
          { 0, 0, -1,   0, 0, -1,     0, 0, -1,
            0, 0, -1,   0, 0, -1,     0, 0, -1,
            0, 1,  0,   0, 1,  0,     0, 1,  0
          };

static std::vector<float> axis_x_vertices = { 0.0f, 0.001f, 0.0f, 
                                              1.0f, 0.001f, 0.0f, }; // moved up a lil - avoids z-fighting with grid lines
static std::vector<float> axis_y_vertices = { 0.0f, 0.0f, 0.0f, 
                                              0.0f, 1.0f, 0.0f, };
static std::vector<float> axis_z_vertices = { 0.0f, 0.001f, 0.0f, 
                                              0.0f, 0.001f, 1.0f, }; // moved up a lil - avoids z-fighting with grid lines
static std::vector<unsigned int> axis_indices = { 0, 1, 2 };

static std::vector<float> rofi_axis_y_vertices = { 0.0f, 0.0f, 0.0f, 
                                                   0.01f, 0.0f, 0.0f, };
static std::vector<float> rofi_axis_z_vertices = { 0.0f, 0.0f, 0.0f, 
                                                   0.0f, 0.01f, 0.0f, };
static std::vector<float> rofi_axis_x_vertices = { 0.0f, 0.0f, 0.0f, 
                                                   0.0f, 0.0f, 0.01f, };
static std::vector<unsigned int> rofi_axis_indices = { 0, 1, 2 };

static std::vector<float> debug_axis_x_vertices = { 0.0f, 0.0f, 0.0f, 
                                                   0.01f, 0.0f, 0.0f, };
static std::vector<float> debug_axis_y_vertices = { 0.0f, 0.0f, 0.0f, 
                                                   0.0f, 0.01f, 0.0f, };
static std::vector<float> debug_axis_z_vertices = { 0.0f, 0.0f, 0.0f, 
                                                   0.0f, 0.0f, 0.01f, };
static std::vector<unsigned int> debug_axis_indices = { 0, 1, 2 };
}

#endif