#ifndef EBO_INCLUDED
#define EBO_INCLUDED

#include <utils/assumptions.hpp>

#include <vector>

class EBO
{
public:
    unsigned int ID;

    EBO();
    ~EBO();

    void deleteBuffer();
    void sendData(std::vector<unsigned int> const &indices);
};


#endif