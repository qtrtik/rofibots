#ifndef CONSTANTS_INCLUDED
#define CONSTANTS_INCLUDED

#include <osi/opengl.hpp>

extern const unsigned int SHD_MODEL_MAT;
extern const unsigned int SHD_VIEW_MAT;
extern const unsigned int SHD_PROJ_MAT;

extern const unsigned int SHD_OBJECT_COLOR;
extern const unsigned int SHD_TEXTURE_BOOL;

extern const unsigned int SHD_CAMERA;
extern const unsigned int SHD_OBJECT_MATERIAL;
extern const unsigned int SHD_LIGHTS;
extern const unsigned int SHD_ALBEDO_TEX;
extern const unsigned int SHD_SKYBOX_TEX;

#endif