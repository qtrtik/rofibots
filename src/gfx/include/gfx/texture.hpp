#ifndef TEXTURE_INCLUDED
#define TEXTURE_INCLUDED

#include <osi/opengl.hpp>

#include <filesystem>
#include <memory>
#include <vector>

class Texture;

using texture_ptr = std::shared_ptr<Texture>;

class Texture
{
    GLuint ID;

public:

    Texture() : ID{0} {};
    Texture(GLuint _ID) : ID{_ID} {};
    GLuint getID() const { return ID; }

    static texture_ptr loadTexture(const std::filesystem::path texture_file);
    static texture_ptr loadTexture(const std::vector<std::filesystem::path> &cube_map_texture_files);
};

#endif