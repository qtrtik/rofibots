#ifndef VBO_INCLUDED
#define VBO_INCLUDED

#include <utils/assumptions.hpp>

#include <vector>

class VBO
{
public:
    unsigned int ID;

    VBO();
    ~VBO();

    void deleteBuffer();
    void sendData(std::vector<float> const &vertices);
};


#endif