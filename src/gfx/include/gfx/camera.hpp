#ifndef CAMERA_INCLUDED
#define CAMERA_INCLUDED

#include <common/node.hpp>
#include <common/objectbase.hpp>
#include <utils/math.hpp>

#include <memory>

class Camera : public ObjectBase
{
    float FOV = 45.0f;
    glm::u32vec2 window_size = glm::u32vec2{0, 0};
    bool is_perspective = true;

    node_weak_ptr node;

public:
    Camera(float _FOV, node_weak_ptr _node);

    float getFOV() const;
    void setFOV(float _FOV);

    glm::vec2 getWindowSize() const;
    void setWindowSize(glm::u32vec2 _window_size);

    bool isPerspective() const { return is_perspective; }
    void setPerspective(bool _is_perspective) { is_perspective = _is_perspective; }

    node_ptr getNode() const
    {
        node_ptr f = node.lock();
        return f;
    }
};

using camera_ptr = std::shared_ptr<Camera>;

#endif