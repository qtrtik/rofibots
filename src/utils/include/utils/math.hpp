#ifndef UTILS_MATH_HPP_INCLUDED
#   define UTILS_MATH_HPP_INCLUDED

#   include <glm/glm.hpp>
#   include <glm/gtc/matrix_access.hpp>
#   include <glm/gtc/quaternion.hpp>
#   include <glm/gtc/constants.hpp>
#   include <glm/gtc/type_ptr.hpp> 

// Added later
#ifndef GLM_ENABLE_EXPERIMENTAL
#define GLM_ENABLE_EXPERIMENTAL
#endif
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/intersect.hpp>
#include <glm/gtx/quaternion.hpp>
#include "glm/gtx/string_cast.hpp"
#include "glm/gtc/epsilon.hpp"
#include <glm/gtx/vector_angle.hpp>

#include <cmath>

#endif
