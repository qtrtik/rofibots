#include <studio/program_info.hpp>

namespace studio {

std::string  get_program_name()
{
    return "RoFIbotStudio";
}

std::string  get_program_version()
{
    return "0.1";
}

std::string  get_program_description()
{
    return  "RoFIbotStudio is a playground experiments with robots composed\n"
            "of multiple autonomous units.";
}

}
