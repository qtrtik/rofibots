#include <studio/program_options.hpp>
#include <studio/program_info.hpp>
#include <utils/assumptions.hpp>
#include <stdexcept>
#include <iostream>

namespace studio {

program_options::program_options(int argc, char* argv[])
    : program_options_default(argc, argv)
{
    add_option(
        "scene",
        
        "The disk path of a scene to be loaded.",
        
        "1"
        );
}

static program_options_ptr  global_program_options;

void initialise_program_options(int argc, char* argv[])
{
    ASSUMPTION(!global_program_options.operator bool());
    global_program_options = program_options_ptr(new program_options(argc,argv));
}

program_options_ptr get_program_options()
{
    ASSUMPTION(global_program_options.operator bool());
    return global_program_options;
}

std::ostream& operator<<(std::ostream& ostr, program_options_ptr options)
{
    ASSUMPTION(options.operator bool());
    options->operator<<(ostr);
    return ostr;
}

}
