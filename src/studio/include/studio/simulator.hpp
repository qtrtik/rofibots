#ifndef STUDIO_SIMULATOR_HPP_INCLUDED
#   define STUDIO_SIMULATOR_HPP_INCLUDED

#   include <osi/simulator.hpp>

#include <controls/control_hub.hpp>
#include <edit/editor.hpp>
#include <edit/scene.hpp>
#include <gfx/render.hpp>
#include <gfx/shader.hpp>
#include <rofi/voxel_graph.hpp>

#include <map>
#include <memory>
#include <vector>

namespace studio {

using scene_ptr = std::shared_ptr<Scene>;
using shader_ptr = std::shared_ptr<Shader>;
using ctrlhub_ptr = std::shared_ptr<ControlHub>;
using editor_ptr = std::shared_ptr<edit::Editor>;

struct Simulator : public osi::Simulator
{
    Simulator();
    ~Simulator();

    void update() override;
    void present() override;

    // call editor
    // call controls
    // call scene

private:
    /* GRAPHICS */
    OpenGLContext gfx_context = OpenGLContext();
    
    /* SCENE */
    scene_ptr scene = std::make_shared<Scene>();

    /* SHADERS */
    std::map<std::string, shader_ptr> my_shaders;

    /* CONTROLS */
    ctrlhub_ptr controls = std::make_shared<ControlHub>();

    /* EDITOR */
    editor_ptr editor = std::make_shared<edit::Editor>(keyboard(), mouse(), timer(), window(), scene, controls);
};
}

#endif
