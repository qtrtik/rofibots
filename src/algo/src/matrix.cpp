#include <algo/matrix.hpp>



glm::mat4 get_view_matrix(camera_ptr camera)
{
    return glm::lookAt(camera->getNode()->getPositionWorld(), 
                       camera->getNode()->getPositionWorld() + -camera->getNode()->getRotationAxisZWorld(), 
                       glm::vec3(0.0f, 1.0f, 0.0f));
}

glm::mat4 get_projection_matrix(camera_ptr camera)
{
    return camera->isPerspective() ? 
            glm::perspective(glm::radians(camera->getFOV()), 
                             camera->getWindowSize().x / camera->getWindowSize().y, 
                             0.1f, 
                             100.0f) 
            : 
            glm::ortho(-camera->getWindowSize().x / 2.0f / 128.0f, camera->getWindowSize().x / 2.0f / 128.0f, 
                       -camera->getWindowSize().y / 2.0f / 128.0f, camera->getWindowSize().y / 2.0f / 128.0f,
                       0.1f,
                       100.0f);
}