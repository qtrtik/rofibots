#ifndef CURSOR_INCLUDED
#define CURSOR_INCLUDED

#include <common/node.hpp>
#include <edit/scene.hpp>
#include <osi/mouse.hpp>
#include <osi/window.hpp>
#include <utils/math.hpp>

#include <array>
#include <memory>
#include <utility>
#include <vector>

using scene_ptr = std::shared_ptr<Scene>;

std::pair<float, float> intersect_axis(float ray_direction_a, float ray_position_a, float LH_first_a, float LH_second_a);
std::pair<float, float> intersect(glm::vec3 ray_position, glm::vec3 ray_direction, std::pair<glm::vec3, glm::vec3> LH);
std::pair<glm::vec3, node_ptr> find_intersection(const std::vector<node_ptr> &scene, glm::vec3 ray_position, glm::vec3 ray_direction);

std::pair<glm::vec3, std::array<glm::vec3, 4>> find_cube_side(node_ptr node, glm::vec3 hit_coords);

std::pair<glm::vec3, glm::vec3> get_pick_ray(const scene_ptr &scene, const osi::Mouse &mouse, const osi::Window &window);
float get_horizontal_intersect_parameter(node_ptr node, const glm::vec3 ray_position, const glm::vec3 ray_direction);
glm::vec3 get_horizontal_plane_intersect(node_ptr node, const glm::vec3 ray_position, const glm::vec3 ray_direction);
float get_vertical_intersect_parameter(node_ptr node, const glm::quat &camera_rotation, const glm::vec3 ray_position, const glm::vec3 ray_direction);
glm::vec3 get_vertical_plane_intersect(node_ptr node, const glm::quat &camera_rotation, const glm::vec3 ray_position, const glm::vec3 ray_direction);

#endif