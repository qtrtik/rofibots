#ifndef MATRIX_INCLUDED
#define MATRIX_INCLUDED

#include <gfx/camera.hpp>
#include <utils/math.hpp>

glm::mat4 get_view_matrix(camera_ptr camera);
glm::mat4 get_projection_matrix(camera_ptr camera);

#endif