#ifndef AABB_INCLUDED
#define AABB_INCLUDED

#include <common/node.hpp>
#include <utils/math.hpp>

#include <array>
#include <utility>
#include <vector>

class AABB
{
    std::pair<glm::vec3, glm::vec3> LH;
    std::array<glm::vec3, 8> full_bbox;

    void findLH(std::vector<float> const &vertices);
    void createFullAABB();

public:
    AABB(std::vector<float> const &vertices);
    AABB(glm::vec3 low, glm::vec3 high);
    std::pair<glm::vec3, glm::vec3> const &getLH() const { return LH; }
    std::array<glm::vec3, 8> const &getFullAABB() const { return full_bbox; }
    std::vector<float> getVertices() const;
    std::vector<unsigned int> getIndices() const;

    std::pair<glm::vec3, glm::vec3> getLHWorld(node_ptr node) const;
    std::pair<glm::vec3, glm::vec3> getAALHWorld(node_ptr node) const;
    std::array<glm::vec3, 8> getAABBWorld(node_ptr node) const;
};

#endif