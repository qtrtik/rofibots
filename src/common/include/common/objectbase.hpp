#ifndef OBJECTBASE_INCLUDED
#define OBJECTBASE_INCLUDED

// Encapsulates Mesh, Camera and Light classes
class ObjectBase
{
public:
    virtual ~ObjectBase() = default;
};

#endif