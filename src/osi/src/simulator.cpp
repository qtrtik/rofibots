#include <osi/simulator.hpp>

namespace osi {

void Simulator::__update()
{
    if (!timer().paused())
        update();
    present();
}

}
