#ifndef OSI_MOUSE_HPP_INCLUDED
#   define OSI_MOUSE_HPP_INCLUDED

#   include <utils/math.hpp>
#   include <string>
#   include <unordered_set>

namespace osi {

struct Config;
void run(Config const&);

struct Mouse final
{
    Mouse()
        : m_pos { 0, 0 }
        , m_pos_delta { 0, 0 }
        , m_just_pressed {}
        , m_just_released {}
        , m_down {}
    {}

    glm::i32vec2 const& pos() const { return m_pos; }
    glm::i32vec2 const& pos_delta() const { return m_pos_delta; }
    std::unordered_set<std::string> const& just_pressed() const { return m_just_pressed; }
    std::unordered_set<std::string> const& just_released() const { return m_just_released; }
    std::unordered_set<std::string> const& down() const { return m_down; }

private:
    friend void ::osi::run(::osi::Config const&);
    glm::i32vec2 m_pos;
    glm::i32vec2 m_pos_delta;
    std::unordered_set<std::string> m_just_pressed;
    std::unordered_set<std::string> m_just_released;
    std::unordered_set<std::string> m_down;
};

}

#endif
