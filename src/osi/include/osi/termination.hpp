#ifndef OSI_TERMINATION_HPP_INCLUDED
#   define OSI_TERMINATION_HPP_INCLUDED

namespace osi {

struct Termination final
{
    Termination()
        : m_quit_flag { false }
    {}

    bool quit_flag() const { return m_quit_flag; }
    void set_quit_flag() { m_quit_flag = true; }

private:
    bool m_quit_flag;
};

}

#endif
