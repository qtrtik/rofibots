#ifndef OSI_RUN_HPP_INCLUDED
#   define OSI_RUN_HPP_INCLUDED

#   include <utils/math.hpp>
#   include <string>
#   include <vector>
#   include <functional>
#   include <filesystem>
#   include <memory>

namespace osi {

struct Simulator;

struct Config final
{
    std::filesystem::path data_root_dir { "./data/" };

    std::string window_title { "RoFIbots" };
    glm::u32vec2 window_pos { 0U, 0U };
    glm::u32vec2 window_size { 1280U, 720U };
    bool resizable { true }; // Used only if 'windowed' is true.
    bool windowed { true }; // When set to false, then 'window_size' is ignored and the desktop size is used instead.
    bool v_sync { false };

    float max_dt { 1.0f / 10.0f };
    float min_dt { 1.0f / 100.0f };
    std::uint32_t cpu_yield_ms { 1U };

    std::uint8_t num_warm_up_iterations { 10U };

    std::filesystem::path icon_relative_path {};
    std::filesystem::path font_relative_path {};
    float font_pixel_size { 16.0f };

    std::function<std::unique_ptr<Simulator>()> constructor;
};

void run(Config const&);

}

#endif
