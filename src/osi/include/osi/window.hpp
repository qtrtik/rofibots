#ifndef OSI_WINDOW_HPP_INCLUDED
#   define OSI_WINDOW_HPP_INCLUDED

#   include <utils/math.hpp>

namespace osi {

struct Config;
void run(Config const&);

struct Window final
{
    Window()
        : m_size { 1280U, 720U }
        , m_is_resized { false }
        , m_is_minimized { false }
        , m_has_keyboard_focus { true }
        , m_is_mouse_in_window { true }
        , m_pixel_size{ 0.345 / 1920.0, 0.195 / 1080.0 }
    {}

    glm::u32vec2 const& size() const { return m_size; }
    bool is_resized() const { return m_is_resized; }
    bool is_minimized() const { return m_is_minimized; }
    bool has_keyboard_focus() const { return m_has_keyboard_focus; }
    bool is_mouse_in_window() const { return m_is_mouse_in_window; }

    // According to my investigation SDL2 does not provide this information.
    // Therefore, this method returns the same constant size for all displays and resolutions.
    glm::vec2 const& pixel_size_in_meters() const { return m_pixel_size; }

    glm::vec2 to_absolute_pixels(glm::vec2 const& relative_coords_in_range_01) const
    { return glm::vec2{ size() } * relative_coords_in_range_01; }

    glm::vec2 to_absolute_meters(glm::vec2 const& relative_coords_in_range_01) const
    { return to_absolute_pixels(relative_coords_in_range_01) * pixel_size_in_meters(); }

private:
    friend void ::osi::run(::osi::Config const&);
    glm::u32vec2 m_size;
    bool m_is_resized;
    bool m_is_minimized;
    bool m_has_keyboard_focus;
    bool m_is_mouse_in_window;
    glm::vec2 m_pixel_size;
};

}

#endif
