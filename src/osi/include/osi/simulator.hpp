#ifndef OSI_SIMULATOR_HPP_INCLUDED
#   define OSI_SIMULATOR_HPP_INCLUDED

#   include <osi/window.hpp>
#   include <osi/keyboard.hpp>
#   include <osi/mouse.hpp>
#   include <osi/timer.hpp>
#   include <osi/termination.hpp>
#   include <utils/math.hpp>

namespace osi {

struct Config;
void run(Config const&);

struct Simulator
{
    virtual ~Simulator() {}

    Window const& window() { return m_window; }
    Keyboard const& keyboard() { return m_keyboard; }
    Mouse const& mouse() { return m_mouse; }
    Timer const& timer() { return m_timer; }
    Termination const& termination() { return m_termination; }

    virtual void update() = 0; // Update the state (i.e., transform objects, create/destroy objects, ... )
    virtual void present() = 0; // Present the state to the user (i.e., render objects)

private:
    friend void ::osi::run(::osi::Config const&);

    void __update();

    Window m_window;
    Keyboard m_keyboard;
    Mouse m_mouse;
    Timer m_timer;
    Termination m_termination;
};

}

#endif
