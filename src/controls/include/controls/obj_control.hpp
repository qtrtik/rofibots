#ifndef OBJ_CONTROL_INCLUDED
#define OBJ_CONTROL_INCLUDED

#include <common/node.hpp>
#include <controls/control.hpp>
#include <osi/mouse.hpp>
#include <utils/math.hpp>

#include <string>
#include <unordered_set>

class ObjectController : public Controller
{
public:
    using Controller::Controller;

    glm::vec3 getFrontVec() const override { return glm::vec3(0.0f, 0.0f, -1.0f); }
    glm::vec3 getUpVec() const override { return glm::vec3(0.0f, 1.0f, 0.0f); }

    void forward() override;
    void backward() override;
    void left() override;
    void right() override;
    void up() override;
    void down() override;

    void mouseRotate(const osi::Mouse &mouse);
    void mouseHorizontalMove(const glm::vec3 &ray_position, const glm::vec3 &ray_direction, glm::vec3 &prev_plane_intersect, bool &object_moving);
    void mouseElevationMove(const glm::vec3 &ray_position, const glm::vec3 &ray_direction, const glm::quat &camera_rotation, glm::vec3 &prev_plane_intersect, bool &object_moving);
};

glm::vec3 make_path(glm::vec3 &new_pos, glm::vec3 &prev_plane_intersect, bool &object_moving);


#endif