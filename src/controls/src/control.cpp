#include <controls/control.hpp>

void Controller::setStep(float dt)
{
    step = speed * dt;
}
void Controller::setSpeed(float _speed)
{
    speed = _speed;
}

void Controller::setup(float dt, node_ptr node)
{
    setNode(node);
    setStep(dt);
}

void Controller::keyboardMove(const std::unordered_set<std::string> &held_keys, 
                              const std::unordered_set<std::string> &pressed_keys)
{
    if (held_keys.contains("W"))
        forward();
    if (held_keys.contains("S"))
        backward();
    if (held_keys.contains("A"))
        left();
    if (held_keys.contains("D"))
        right();
    if (held_keys.contains("R"))
        up();
    if (held_keys.contains("F"))
        down();
}

void Controller::forward()
{
    bound_node->translatePosVec(step * getFrontVec());
}
void Controller::backward()
{
    bound_node->translatePosVec(-step * getFrontVec());
}
void Controller::left()
{
    bound_node->translatePosVec(-step * glm::normalize(glm::cross(getFrontVec(), getUpVec())));
}
void Controller::right()
{
    bound_node->translatePosVec(step * glm::normalize(glm::cross(getFrontVec(), getUpVec())));
}
void Controller::up()
{
    bound_node->translatePosVec(step * getUpVec());
}
void Controller::down()
{
    bound_node->translatePosVec(-step * getUpVec());
}

void Controller::rotate(glm::vec3 rotational_difference)
{
    bound_node->rotateRotationQuat(rotational_difference);
}