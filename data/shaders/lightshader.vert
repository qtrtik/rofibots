#version 450
layout(binding = 0, std140) uniform Camera {
    mat4 view;
    mat4 projection;
    vec4 position;
} camera;

layout(location = 0) uniform mat4 model;

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texture_coordinate;

layout(location = 0) out vec3 frag_position;
layout(location = 1) out vec3 frag_normal;
layout(location = 2) out vec2 frag_texture_coordinate;

void main()
{
    frag_normal = vec3(model * vec4(normal, 0.0));
    frag_position = vec3(model * vec4(position, 1.0));
    frag_texture_coordinate = texture_coordinate;
    
    gl_Position = camera.projection * camera.view * vec4(frag_position, 1.0);
}
