#version 450

layout(binding = 0, std140) uniform Camera {
	mat4 view;
	mat4 projection;
	vec3 position;
} camera;

layout(location = 0) in vec3 position;

layout(location = 0) out vec3 frag_texture_coordinate;

void main()
{
    frag_texture_coordinate = position;
	mat4 viewWithoutTranslation = mat4(mat3(camera.view));
    gl_Position = camera.projection * viewWithoutTranslation * vec4(position, 1.0);
}  