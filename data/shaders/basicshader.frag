#version 450
layout(location = 3) uniform vec4 object_color;

layout(location = 0) out vec4 final_color;

void main()
{
    final_color = object_color;
}
