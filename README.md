# **RoFIbots** project

**RoFIbots** is a codebase and playground for scientific experiments and evaluations
of planning strategies for robots composed of multiple autonomous units.

## License

**RoFIbots** is available under the **zlib** license. It is included as 
file `LICENSE.txt` into the repository: https://gitlab.fi.muni.cz/qtrtik/rofibots

## Target platforms

The primary target platform is PC running either Windows 10 (or later) or Ubuntu
22.04 (or later) operating systems.

## Software dependencies

The following software must be installed on your computer before you can
start with the **RoFIbots** project:
- **git** distributed version control system: https://git-scm.com/
    - (optional) Configure your git in a console using these commands: 
        ```
        git config --global user.name "Your name"
        git config --global user.email "Your email"
        git config --global core.autocrlf false
        git config --global core.filemode false
        git config --global color.ui true
        ```
- **C++ compiler** supporting at least **C++20** standard:
    - On Ubuntu use one of these two options:
        - **g++** of GCC: https://en.wikipedia.org/wiki/GNU_Compiler_Collection
        - **Clang**: https://clang.llvm.org/ and https://en.wikipedia.org/wiki/Clang
        - NOTE: Consider using this command for installing the compiler:
            ```
            sudo apt install gcc g++
            ```
    - On Windows use the **Microsoft C++** compiler and debugger:
        1. Go to page: https://visualstudio.microsoft.com/downloads/#other
        2. Search for **Tools for Visual Studio 2022** and click on the text to open nested items.
        3. Search for **Build Tools for Visual Studio 2022** nested item and click on the
           **Download** button.
- **CMake** build system: https://cmake.org/
  - NOTE: On Ubuntu consider using this command:
    ```
    sudo apt install make cmake ninja-build
    ```
- **vcpkg** software package manager: https://github.com/microsoft/vcpkg
  - NOTE: On Ubuntu we recommend to use this command first:
    ```
    sudo apt install curl pkg-config libgl1-mesa-dev libegl1-mesa-dev
    ```
  - Once you have the package manager installed, install into it required packages:\
    `vcpkg install sdl2 glad glm boost gtest imgui[opengl3-binding,sdl2-binding] --clean-after-build`\
    On Windows append the option `--triplet=x64-windows` to the command and `--triplet=x64-linux` on Ubuntu.
- **Microsoft Visual Studio Code** (VS code) source-code editor: https://code.visualstudio.com/
    - Once you have the editor installed, install into it these extensions:
        - **C/C++** by Microsoft: https://github.com/microsoft/vscode-cpptools
        - **C/C++ Extension Pack** by Microsoft: https://github.com/microsoft/vscode-cpptools
        - **C/C++ Themes** by Microsoft: https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools-themes
        - **CMake Tools** by Microsoft: https://github.com/microsoft/vscode-cmake-tools
        - (optional) **CMake** by twxs: https://github.com/twxs/vs.language.cmake
        - (optional) **Git Graph** by mhutchie: https://github.com/mhutchie/vscode-git-graph
        - (optional) **Code Spell Checker** by Street Side Software: https://github.com/streetsidesoftware/vscode-spell-checker
    - It is common and useful to use `launch.json` config file for launching an
        executable. That way you can specify command-line parameters for the
        executable. The initial (minimal) version is under `setup` folder. You
        only need to copy the file from the `setup` folder to the folder
        `.vscode` (create this folder, if it does not exist). 
- (optional) **SmartGit** Git GUI client: https://www.syntevo.com/smartgit/

## Downloading **RoFIbots**

We do not provide **RoFIbots** in binary form. That means you must
download the source code and then build it.

The recommended way how to obtain source code is via `Git`. You can
either clone or fork **RoFIbots**'s repository. Cloning is recommended for
a member of **RoFIbots** project with Developer rights. Forking is then for
everyone else. Both procedures are described is subsections below.

NOTE: Alternatively, you can also download a ZIP package with the source
code from the projects web: https://gitlab.fi.muni.cz/qtrtik/rofibots 

### Cloning

Create a new directory on the disk for **RoFIbots**. Let `<rofibots-root-dir>`
be the full path to that directory. Now open the console and type
there these commands:
```
cd <rofibots-root-dir>
git clone https://gitlab.fi.muni.cz/qtrtik/rofibots.git . 
```

### Forking

First you need to go to GitLab and make a fork of **RoFIbots** project:
- Go to https://gitlab.fi.muni.cz/qtrtik/rofibots
- Click on the **Fork** button at the upper-right corner of the page.
- Put it all information requested in the form.
- Click on the **Fork project** button.

Now clone the forked project. The procedure is the same as in the `Cloning`
subsection above, except the URL in the `git clone` command, which must
reference your forked repository.

## Integrating **vcpkg**

Before we can build **RoFIbots** in VS Code, we must let VS Code to know
where is **vcpkg** installed (because it contains SW packages **RoFIbots**
needs during the build process). We must create file

```
<rofibots-root-dir>/.vscode/settings.json
```

with this content:

```
{
    "cmake.configureSettings": {
        "CMAKE_TOOLCHAIN_FILE": "<vcpkg-install-dir>/scripts/buildsystems/vcpkg.cmake",
        "CMAKE_BUILD_TYPE": "${buildType}"
    }
}
```
where `<vcpkg-install-dir>` must be replaced by the actuall installation directory of **vcpkg**.

NOTE: When working on Windows, VS Code may have created a "global" 
settings file here:
```
<user-dir>/AppData/Roaming/Code/User/settings.json
```
Instead of creating the new settings file as described above, you
can just update this existing "global" setting file by adding the section:
```
    "cmake.configureSettings": {
        "CMAKE_TOOLCHAIN_FILE": "<vcpkg-install-dir>/scripts/buildsystems/vcpkg.cmake",
        "CMAKE_BUILD_TYPE": "${buildType}"
    }
```
The advantage of this approach is, that the **vcpkg** integration
to VS Code would work for all CMake C++ projects on your computer
(including **RoFIbots** of course).

## Building **RoFIbots**

Open **Microsoft Visual Studio Code** and in the main menu choose:
`File/Open Folder...` and open the **RoFIbots**'s directory `<rofibots-root-dir>`.

Now you should be able to build **RoFIbots** the same way as any other
CMake C++ application. All needed information are available here:
https://code.visualstudio.com/docs/cpp/introvideos-cpp

Once you successfully build the `install` target, then you can find
all binaries ready for project's distribution under the `dist` directory.
